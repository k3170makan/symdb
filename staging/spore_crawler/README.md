# Spore Crawler : Symbolic Taint Analysis with Angr

Spore Crawler is a simple extension to angr that implements taint analysis based on claripy's annotations.
To get it going simply use the -b switch and pass it an executable ELF file you'd like to analysize, Spore Crawler
will run with some pre-cooked opts to get you going. To tune crawlers performance and extend its reach in the binary
check out the other options.

## User Guide

```
usage: spore_crawler.py [-h] [-D] [-b BINARY] [-c ARG_COUNT] [-l ARG_LENGTH] [-L LIMIT] [-s STEP_COUNT] [--stdin STDIN]

optional arguments:
  -h, --help            show this help message and exit
  -D, --directed_exec   Use directed symbolic execution
  -b BINARY, --binary BINARY
                        Binary to parse
  -c ARG_COUNT, --arg_count ARG_COUNT
                        Number of arguments
  -l ARG_LENGTH, --arg_length ARG_LENGTH
                        Bit length of each argument
  -L LIMIT, --limit LIMIT
                        maximum number of states to keep alive during depth first search
  -s STEP_COUNT, --step_count STEP_COUNT
                        Number of symbolic execution steps
  --stdin STDIN         symbolize stdin, argument is the length of stdin as a byte-wise claripy.BVS

```
**please note this is a research tool and is still a work in progress** 

## Taint analysis with SporeCrawler

Spore Crawler offers are few simple options for configuring a taint analysis run.
Because we are symbolizing arguments given to the binary; you can configure the count and byte length of each.

See the following example run on /usr/bin/vstp:
```
>$ ./spore_crawler.py -b /usr/bin/vstp -c 8 -l 16 --stdin 16 -L 5 -s 1024
|--binary :=> /usr/bin/vstp
|--arg_count :=> 8
|--arg_length :=> 16
|--[<SimState @ 0x5000e0>]> memcpy (dest=[576460752303357456], src=[2...], n=[0])
		|--- arg_0 :=> b'vstpp\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_1 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_2 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_3 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_4 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_5 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_6 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_7 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- tainted regs : rsi rdi rsi rbp rsp rip rax rbx rcx rdx r10 r12 r13 r14 r15 
	|-- satisfiable : True 
		|--- stdin :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 

```

In the output above we can see a memcpy that spore crawler says can be influenced by our input.
We can also see the arguments (the ARGV array "arg_1 ... arg_7" ) fed to the binary in order to reach that execution (the test case).
So basically if you want to fuzz to target that memcopy you now know what input you should give ARGV and STDIN.

During a run you may see a status bar show with some helpful metrics to let you know how your run is going:

```
[/usr/bin/gnuplot>] {798} (5) <SimState @ 0x533bf5> -11-
[] : the binary under analysis
{} : symbolic steps executed
() : active symbolic states under execution
<> : top active state
-- : deadedned states
```

### Test Case Generation

Because Spore Crawler uses symbolic execution, its pretty swift at working out arguments that make up command line options:

```
|--binary :=> /usr/bin/gnuplot
|--arg_count :=> 8
|--arg_length :=> 16
|--[<SimState @ 0x601518>]> memcpy (dest=[3221229344], src=[576460752303357648...], n=[64])
		|--- arg_0 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_1 :=> b'--h\x80\x80\x80 \x80\x80\x02\x00\x80tin' 
		|--- arg_2 :=> b'-p\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_3 :=> b'--r\x01\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80\x80' 
		|--- arg_4 :=> b'-p\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_5 :=> b'--vers\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_6 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_7 :=> b'-p\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- tainted regs : rsi rdi rsi rbp rsp rip rax rbx rcx rdx r10 r12 r13 r14 r15 
	|-- satisfiable : True 
		|--- stdin :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 

```
We can see it discovered some command line options in the gnuplot binary.

Beyond that spore crawler also tries to resolve data involved in some functions this can unearth some insights that may help
in reverse engineering a binary. Take a look at these intersting strings pulled out of some binaries under analysis:

```
|--binary :=> /usr/bin/dirmngr
|--arg_count :=> 8
|--arg_length :=> 16
|--[<SimState @ 0x500390>]> strncpy (dest=['0x48ffe0'], src=[b'**dirmngr\x00flush the cache**\x...],  n=[79])
		|--- arg_0 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_1 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_2 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_3 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_4 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_5 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_6 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- arg_7 :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 
		|--- tainted regs : rsi rdi rsi rbp rsp rip rax rbx rcx rdx r10 r12 r13 r14 r15 
	|-- satisfiable : True 
		|--- stdin :=> b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' 

```

In the above output spore crawler evaluated a string as the src of tainted strncpy of 79 bytes.



