# SymDb : Symbolic DeBugging (Angr + Python GDB)

Is a GDB extension that wraps a useful interface around angr to make for very powerful gdb extensions. What were doing here
is basically allowing gdb to set the stage for symbolic exploration of a binary, this allows users to inteface with functionality
such as a automated test case generation and taint analysis (more features incomming!). If this is a little over your head or out
of your field, please check out the Usage guide it will include some pretty easy to understand examples.

** this is currently a work in progress although the tools here can still help reverse engineers quite profoundly for small enough binaries, improvements and bug fixes on the way!

 Thanks to the folks who made angr, vex / halgrind all the cool stuff this project depends on.

# Install
Installing SymDb and using it is prettys stratigh forward, all we need to do is make sure all the back-ends
are installed, namely angr and python's gdb interface. 

1. Install angr
[finish later]
2. Install pygdb
[finish later]
3. Install symdb for gdb
[finish later]

