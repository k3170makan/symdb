import gdb
import os
import argparse
import angr
import claripy
import time
import re
import logging
import subprocess

REGNAMES = ["rip","rax","rbx","rcx","rdx","rsi","rdi","rbp","rsp","r9","r8""r10","r11","r12","r13","r14","r15","cs","ds","es","fs","gs","eflags"]
ARG_COUNT=5
ARG_SIZE=10
TARGET="rip#"

class SymbolicDebuggingHelp(gdb.Command):
	def __init__(self):
		super().__init__("info psydb help",gdb.COMMAND_USER)
	def invoke(self):
		pri
class SymbolicDebugging(gdb.Command):
	def __init__(self):
		super().__init__("psydb-search",gdb.COMMAND_DATA)
		self.args = None
		self.is_env = False
		self.filename = ""
		self.base_address = 0
		self.target = 0
		self.sym_env = None
		self.env_dict = {}		
		self.usage = "[Usage]: psydb_search [address]/[reg]\n\t* - show symbolic constraints, and a sample test case needed to reach block with a given address\n\t* - [address]/[reg] optional integer address / register name to get constraints for. By default symbdb will use the current rip value.\n"
		self.parser = argparse.ArgumentParser()
		
		self.parser.add_argument("-t","--target",
						help="address or register to symbolize",
						type=type(""),	
						default="rip")
		self.parser.add_argument("-c","--argn",
						help="number of argv arguments to symbolize",
						type=int,
						default=3)
		self.parser.add_argument("-s","--args",	
						help="size of each argument in bytes",
						type=int,
						default=20)

		self.parser.add_argument("-b","--binary",help="use this binary file",type=type(""))
		self.parser.add_argument("-a","--arguments",help="symbolize ARGV arguments",action='store_true',default=False)
		self.parser.add_argument("-1","--stdin",action='store_true',help="symbolize stdin",default=False)
		self.parser.add_argument("-e","--env",help="symbolize a specified environment",type=type(""))
		self.parser.add_argument("-p","--pipe",help="symbolize a give pipe",type=type(""))
				
	def _check_args(self,args):
		try:
			self.args = self.parser.parse_args(args)
		except:
			return False	
		if (self.args):
			if (self.args.binary):
				self.filename = self.args.binary
			else:
				self.filename = self.get_binary_filename(False)
			if (self.args.env):
				if (self.args.env != "all"):
					self.sym_env = self.env_dict
					name = self.args.env

					try:
						literal = self.env_dict[name]
						self.sym_env[name] = claripy.BVS("%s" % (name),len(literal)*0x8)
					except KeyError:
						self.sym_env.update({name: claripy.BVS("%s" % (name),16*0x8)})
		return False

	def get_env(self,tty):
		out = gdb.execute("show environment",from_tty=tty,to_string=True)
		for var_line in out.split("\n"):
			_var = var_line.split("=")
			if (len(_var) == 2):
				self.env_dict[_var[0]]=_var[1]
		
	def get_binary_filename(self,tty):
		out = gdb.execute("info files",from_tty=False,to_string=True)
		top_line = out.split("\n")[0]
		filename = top_line.split("\"")[1]
		return filename

	def get_reg_value(self, reg, tty):

		out = ""
		value = ""
		if self.is_reg(reg):
			out = gdb.execute("print /x $%s" % (reg),from_tty=False,to_string=True)
			value = int(out.split("=")[1].replace(' ','').replace('\n',''),16);	
		return value

	def get_base_address(self,tty):
		out = ""
		value = ""
		try:
			out = gdb.execute("info inferior",from_tty=False,to_string=True) #assuming 1 inferior
			pid = out.split("\n")[1].split(" ")[6]
			#print("[*] pid :=> %s" % (pid))	
		except:
			return 0
		output = subprocess.check_output(["/usr/bin/pmap","-x", "%s" % (pid)])
		base = output.decode("utf-8").split("\n")[2].split(" ")[0].replace(' ','')
		return int(base,16)

	def is_reg(self,reg):
		return reg in REGNAMES

	def symbolic_search(self,target):
	

		arg_size = ARG_SIZE
		arg_count = ARG_COUNT
		self.project = angr.Project(self.filename,
					load_options={"auto_load_libs":False,
							"main_opts":{"base_addr":self.base_address,
									"force_rebase":True
							}})

		if self.args:
			arg_size = self.args.args
			arg_count = self.args.argn	

		self.sym_args = [claripy.BVS("arg_%d" % (i),arg_size*0x8) for i in range(arg_count)]
		self.sym_stdin = claripy.BVS("stdin",0x8*128)
		logging.getLogger('angr').setLevel('CRITICAL')
		#entry_state = self.project.factory.full_init_state(args=[self.filename]+self.sym_args,
		print("[*] target : [%s]" % (hex(target)))
		print("[*] base address : [%s]" % (hex(self.base_address)))

		if self.args and self.args.env:
			entry_state = self.project.factory.full_init_state(args=self.sym_args,stdin=self.sym_stdin,env=self.sym_env)
		else:
			entry_state = self.project.factory.full_init_state(args=self.sym_args,stdin=self.sym_stdin)
		self.simmgr = self.project.factory.simulation_manager(entry_state)
		print("[*] searching ...")
		self.simmgr.explore(find=target)

		if len(self.simmgr.found) == 0:
			print("[*] no states found")
		
		for state in self.simmgr.found:
			#	print("\t -- {%s}" % (repr(constraint)[:70]))
			print("args state@[%s]:" % (repr(state)))
			if (self.args):
				if (self.args.arguments == True):
					for arg in self.sym_args:
						print("\t%s := {%s}" % (arg,state.solver.eval(arg,cast_to=bytes)))
				if (self.args.env):
					_env = self.sym_env[self.args.env]
					print("\t%s := {%s}" % (_env,state.solver.eval(_env,cast_to=bytes)))
				if (self.args.stdin == True):
					print("\tstdin := {%s}" % (state.solver.eval(self.sym_stdin,cast_to=bytes)))
			else:
				for arg in self.sym_args:
					print("\t%s := {%s}" % (arg,state.solver.eval(arg,cast_to=bytes)))


	def get_target(self,tty=None,addr=None):
		current_block = 0	
		target = 0
		if self.args != None:
			target = self.args.target
		elif addr != None:
			target = addr
		if target.isalpha() and self.is_reg(target):
			target = self.get_reg_value(target,tty)
		elif type(target) == type("") and target[:2] == "0x":
			target = int(target,16)

		if target == 0:
			target = self.get_reg_value("rip",tty)

		self.target = target
		self.base_address = self.get_base_address(tty)

		if self.base_address == 0:
			raise Exception("couldn't get base address")
		if self.args == None:
			self.filename = self.get_binary_filename(tty)
		return self.target
		
	def invoke(self, args,tty):
		_args = None
		try:
			self.get_env(tty)	
			self._check_args(args.split())
			if len(args.split(" ")) == 1:
				self.symbolic_search(self.get_target(addr=args,tty=tty))
			else:
				self.symbolic_search(self.get_target(tty=tty))

		except Exception as e:
			exc_type, exc_obj,exc_tb = sys.exc_info()
			fname  = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			print(exc_type,fname,exc_tb.tb_lineno)
			print(e)
			print(self.usage)
SymbolicDebugging()
