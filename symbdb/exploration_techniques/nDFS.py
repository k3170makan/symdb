#!/usr/bin/python3
import angr
import random
import claripy

class nDFS(angr.ExplorationTechnique):
	"""
	Depth-first search with configurable alive-path limit
	
	Will only keep n paths active at a time, any others will be stashed in the 'deferred' stash.
	When we run out of active paths to step, we take the longest one from deferred and continue.
	"""
	def __init__(self, limit=1,deferred_stash='deferred'):
		super(nDFS, self).__init__()
		self.limit = limit
		self._random = random.Random()
		self._random.seed(10)
		self.deferred_stash = deferred_stash

	def setup(self, simgr):
		if self.deferred_stash not in simgr.stashes:
			simgr.stashes[self.deferred_stash] = []
	def step(self, simgr, stash='active', **kwargs):
		simgr = simgr.step(stash=stash, **kwargs)
		if len(simgr.stashes[stash]) > 1:
			self._random.shuffle(simgr.stashes[stash])
			simgr.split(from_stash=stash, to_stash=self.deferred_stash, limit=self.limit)
			
		if len(simgr.stashes[stash]) == 0:
			if len(simgr.stashes[self.deferred_stash]) == 0:
				return simgr
		return simgr

