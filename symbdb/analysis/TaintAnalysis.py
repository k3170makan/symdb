#!/usr/bin/python3
import angr
import sys
import claripy
import argparse
import logging

memory_access = [] 
_sym_stdin = None
_args = []
_sym_files = 0
_hooks = 0
_hooks_tainted = 0

REGNAMES = ["rip","rax","rbx","rcx","rdx","rsi","rdi","rbp","rsp","r9","r8""r10","r11","r12","r13","r14","r15","cs","ds","es","fs","gs","eflags"]

"""
	SporeCrawler essentially does taint tracking using angr and claripy's annotations
"""

class SporeCrawler(angr.Analysis):
	def __init__(self,filename=None,initial_state=None,project=None,steps=500,use_dm=False,limit=1):

		self.use_dm = use_dm
		self.limit = limit
		self.filename = filename
		self.steps = steps
		self.project = project
		self.goals = []
		self.deadends = []
		self.target_functions = []
		self.found_targets = False
		self.initial_state = initial_state
		self.CFG_done = False
		self.using_dm = False
		self.diverted = False
		self._init_hooks()
		self._init_target_functions()
		self.dm = angr.exploration_techniques.Director(cfg_keep_states=True)

		self.simmgr = project.factory.simulation_manager(self.initial_state)

		self.simmgr.use_technique(nDFS(self.limit))
		#self.simmgr.use_technique(angr.exploration_techniques.DFS())
		if self.use_dm:
			#print("*> using directed symbolic exploration towards => %s" % (self.target_functions))
			try:
				print("[*] deriving CFG...",end='\r')
				self.project.analyses.CFGFast()
				self.CFG_done = True
				print("[*] CFG derivation successfull!")
			except:
				print("[x] CFGFast failed...")
				self.CFG_done = False

			self._init_goals()
			self.simmgr.use_technique(self.dm)

		#crawler = ["_","_/","_/-","_/--","_/---","_/---\\","/---\\_","---\\_","--\\_","-\\_","\\_"]
		for i in range(steps):
		#try:
			#print("[%s>] %d/%d {%s} (%s) %s -%s-" % (self.filename,
			try:
				print("[%s>] {%s} (%s) %s -%s-" % (self.filename,
							#_hooks_tainted,_hooks,
							i,len(self.simmgr.stashes['active']),
							self.simmgr.stashes['active'][0],
							len(self.simmgr.stashes['deadended'])),end='\r')

				self.simmgr.step()
			except:
				pass
			if memory_access != []:
				if not(self.CFG_done):
					self.project.analyses.CFGFast()
					self.CFG_done = True
				"""
				for deadend in self.simmgr.stashes['deadended']:
					ip = deadend.ip
					if type(ip) != int or type(ip) != type(claripy.BVS()):
							ip = _eval_int(deadend,ip)
					if not(ip in self.deadends):
						self.deadends.append(ip)
						self.append_goal(ip)
				"""

				for state_ip in memory_access:
					if not(state_ip in self.goals):
						self.append_goal(state_ip)				
				
				if self.use_dm and not(self.using_dm):
					print("!> switching to directed symbolic execution...")
					self.simmgr.use_technique(self.dm) #this won't work we need to dynamically add goals
					self.using_dm = True
	
				if len(self.simmgr.stashes['active']) > 1024:
					break
			#except Exception as e:
				#print(e)
	
	
	
	def _init_hooks(self):
		self.project.hook_symbol("fopen",fopen_hook())	
		#self.project.hook_symbol("fscanf",fscanf_hook())	
		#self.project.hook_symbol("fgets",fgets_hook())	
		#self.project.hook_symbol("sprintf",sprintf_hook())	
		#self.project.hook_symbol("snprintf",snprintf_hook())	
		#self.project.hook_symbol("vsprintf",vsprintf_hook())	
		#self.project.hook_symbol("vnsprintf",vnsprintf_hook())	
		#self.project.hook_symbol("getch",getch_hook())	
		#self.project.hook_symbol("getchar",getchar_hook())	
		self.project.hook_symbol("strcpy",strcpy_hook())	
		self.project.hook_symbol("strncpy",strncpy_hook())	
		#self.project.hook_symbol("strlen",strlen_hook())	
		#self.project.hook_symbol("strcat",strcat_hook())	
		#self.project.hook_symbol("strncat",strncat_hook())	
		#self.project.hook_symbol("strcmp",strcmp_hook())	
		self.project.hook_symbol("memcpy",memcpy_hook())	
		#self.project.hook_symbol("memcmp",memcmp_hook())	
		self.project.hook_symbol("memmove",memmove_hook())	
		self.project.hook_symbol("memset",memset_hook())	


		self.target_function_names = ['strcpy','memcpy','strncpy','memmove','memset']

	def _init_target_functions(self):
		for function_name in self.target_function_names:
			_function = self.project.kb.functions.function(name=function_name,plt=False)
			if _function:
				print("*> found function => %s @[%s]" % (function_name, _function))	
				self.target_functions.append(_function.addr)

		if len(self.target_functions) > 0:
			self.found_targets = True	


	def append_goal(self,addr):
		print("*>\t adding goal => %s [%s]" % (type(addr),hex(addr)))
		#_goal = DivertExecuteAddressGoal(addr)
		_goal = angr.exploration_techniques.ExecuteAddressGoal(addr)
		self.goals.append(addr) #goals we are already diverting from
		if self.diverted:
			self.dm.add_goal(_goal)
		else:
			self.dm = angr.exploration_techniques.Director(cfg_keep_states=True, goals=[_goal])
			self.diverted = True

	def _init_goals(self):
		for function in self.target_functions:
			_goal = angr.exploration_techniques.ExecuteAddressGoal(function.addr)
			self.goals.append(_goal)	
			self.dm.add_goal(_goal)


angr.AnalysesHub.register_default("SporeCrawler",SporeCrawler)

class TaintAnnotation(claripy.Annotation):
    def eliminatable(self):
        return False

    def relocatable(self):
        return False


if __name__=="__main__":

	parser = argparse.ArgumentParser()
	
	parser.add_argument("-D","--directed_exec",help="Use directed symbolic execution",action='store_true',default=False)	

	parser.add_argument("-b","--binary",help="Binary to parse")	
	parser.add_argument("-c","--arg_count",help="Number of arguments",default=3)	
	parser.add_argument("-l","--arg_length",help="Bit length of each argument",default=16)	
	parser.add_argument("-L","--limit",help="maximum number of states to keep alive during depth first search",type=int,default=1)	
	parser.add_argument("-s","--step_count",help="Number of symbolic execution steps",default=256)	
	parser.add_argument("--stdin",help="symbolize stdin, argument is the length of stdin as a byte-wise claripy.BVS",default=64,type=type(1))	
	args = parser.parse_args()

	binary_name = args.binary
	arg_count = int(args.arg_count)
	arg_length = int(args.arg_length)
	step_count = int(args.step_count)

	project = angr.Project(binary_name,load_options={"auto_load_libs":False,"main_opts":{"base_addr":0x400000, "force_rebase":False}});
	
	print("|--binary :=> %s" % (binary_name))
	print("|--arg_count :=> %s" % (arg_count))
	print("|--arg_length :=> %s" % (arg_length))

	_args = [claripy.BVS("cmd_arg%s" % (i), 8*arg_length,annotations=(claripy.Annotation(),)) for i in range(arg_count)]
	_sym_stdin = claripy.BVS("sym_stdin", 8*args.stdin,annotations=(claripy.Annotation(),))
	entry_state = project.factory.entry_state(args=_args,stdin=_sym_stdin)
	global_address_access = project.analyses.SporeCrawler(filename=binary_name,
									initial_state=entry_state,
									project=project,
									steps=step_count,
									use_dm=args.directed_exec,
									limit=args.limit)	
