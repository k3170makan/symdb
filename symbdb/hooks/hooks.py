#!/usr/bin/python3
import angr
import claripy

"""
	SporeCrawler essentially does taint tracking using angr and claripy's annotations
"""
class fopen_hook(angr.SimProcedure):
	def run(self, path_name, mode):
		#_path_name = _eval_bytes(self.state, path_name.to_claripy())
		#_path_name = self.state.solver.eval(self.state.memory.load(_path_name,32),cast_to=bytes)
		#_path_name = self.state.solver.eval(path_name.to_claripy(),cast_to=int)
		_mode = _eval_int(self.state, mode.to_claripy())
		
		if(is_tainted(self.state) != None):# and not(_eval_int(self.state,self.state.ip) in memory_access)):
			
			new_path_name = _eval_bytes(self.state, self.state.memory.load(path_name,64))
		
			print("|--[%s]> fopen (path_name=[%s...], mode=[%s])" % (self.state,
							repr(new_path_name)[:30],
							repr(_mode)[:30]))
				
			#memory_access.append(_eval_int(self.state,self.state.ip))
			#return claripy.BVS("sym_file_%s" % (repr(int(1+random.random()*9999%10))),0x8*128,annotations=(claripy.Annotation(),))


			print(type(self.state.ip.to_claripy()))
			eval_args(self.state)
	
class strncpy_hook(angr.SimProcedure):
	def run(self, dest, src, n):
		_dest = dest
		_src = src
		_n = n

		try:
			_dest = hex(_eval_int(self.state, dest.to_claripy()))
			_src = _eval_int(self.state, src.to_claripy())
			_n = _eval_int(self.state, n.to_claripy())
			
			_src = _eval_bytes(self.state, 
						self.state.memory.load(_src,
									32))
			
		except:
			pass

		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("|--[%s]> strncpy (dest=[%s], src=[%s...],  n=[%s])" % (repr(self.state),
					repr(_dest)[:30],
					repr(_src)[:30],
					repr(_n)[:30]))
				


			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))


			new_dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 8,annotations=(claripy.Annotation(),))
			return dest 

class strcpy_hook(angr.SimProcedure):
	def run(self, dest, src):
		_src = src
		_dest = dest
		try:
			_src = _eval_int(self.state, src.to_claripy())
			_dest = hex(_eval_int(self.state, dest.to_claripy()))
			_src = _eval_bytes(self.state.memory.load(src,32),cast_to=bytes)

		except:
			pass

		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("|--[%s]> strcpy (dest=[%s], src=[%s...])" % (repr(self.state),
								repr(_dest)[:30],
								repr(_src)[:30]))
			

			eval_args(self.state)
	
			#_print_constraints(self.state)
			#memory_access.append(_eval_int(self.state,self.state.ip))
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 0x8,annotations=(claripy.Annotation(),))
			return dest 
		


class memmove_hook(angr.SimProcedure):
	def run(self, dest, src, n):

		_dest = dest
		_src = src
		_n = n

		try:
			#_dest = _eval_int(self.state, dest.to_claripy())
			#_src = _eval_int(self.state, src.to_claripy())

			_n = _eval_int(self.state, n.to_claripy())
			_src = _eval_int(self.state, src.to_claripy())
			_dest = hex(_eval_int(self.state, dest.to_claripy()))
			_src = _eval_bytes(self.state.memory.load(src,32),cast_to=bytes)

			#_src = _eval_int(self.state, 
			#			self.state.memory.load(_src,
			#						_n))
		except:
			pass

		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("|--[%s]> memmove (dest=[%s], src=[%s...], n=[%s])" % (repr(self.state),
							repr(_dest)[:30],
							repr(_src)[:30],
							repr(_n)[:30]))			
					

			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 8,annotations=(claripy.Annotation(),))
			return dest 
		

class memset_hook(angr.SimProcedure):
	def run(self, s, c, n):
		_s = s
		try:
			_n = _eval_int(self.state, n.to_claripy())
			_s = self.state.solver.eval(self.state.memory.load(src,32),cast_to=bytes)

		except:
			pass
		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("|--[%s]> memset (s=[%s], c=[%s], n=[%s])" % (repr(self.state),
									repr(_s)[:30],
									repr(c)[:30],
									repr(_n)[:30]))

				
			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 8,annotations=(claripy.Annotation(),))
			return dest 


class memcpy_hook(angr.SimProcedure):
	def run(self, dest, src, n):
		_dest = dest
		_src = src
		_n = n

		try:
			_dest = _eval_int(self.state, dest.to_claripy())
			_src = _eval_int(self.state, src.to_claripy())
			_n = _eval_int(self.state, n.to_claripy())

			#_src = _eval_int(self.state, 
			#			self.state.memory.load(_src,
			#						_n))

			_src = _eval_bytes(self.state.memory.load(src,32),cast_to=bytes)

		except:
			pass

		if(is_tainted(self.state) != None):# not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("|--[%s]> memcpy (dest=[%s], src=[%s...], n=[%s])" % (repr(self.state),
									repr(_dest)[:30],
									repr(_src)[:30],
									repr(_n)[:30]))			

				
			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 
								8,
							annotations=(claripy.Annotation(),))
			return dest 
	
"""
class fscanf_hook():
class fgets_hook():
class sprintf_hook():
class snprintf_hook():
class vsprintf_hook():
class vnsprintf_hook():
class getch_hook():
getchar",getchar_hook())	
strcpy",strcpy_hook())	
strncpy",strncpy_hook())	
strlen",strlen_hook())	
strcat",strcat_hook())	
strncat",strncat_hook())	
strcmp",strcmp_hook())	
memcpy",memcpy_hook())	
memcmp",memcmp_hook())	
memmove",memmove_hook())	
memset",memset_hook())	
"""
